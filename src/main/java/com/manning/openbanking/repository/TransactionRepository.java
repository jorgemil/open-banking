package com.manning.openbanking.repository;

import com.manning.openbanking.entity.Transaction;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends CrudRepository<Transaction, Long> {

    List<Transaction> getTransactionsByAccountNumber(Long accountNumber);

}
