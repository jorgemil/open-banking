package com.manning.openbanking.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonValue;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Status of a transaction entry on the books of the account servicer.
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-01-30T17:41:15.217637300-05:00[America/Bogota]")
public enum OBEntryStatus1Code {
  
  BOOKED("Booked"),
  
  PENDING("Pending"),
  
  REJECTED("Rejected");

  private String value;

  OBEntryStatus1Code(String value) {
    this.value = value;
  }

  @JsonValue
  public String getValue() {
    return value;
  }

  @Override
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static OBEntryStatus1Code fromValue(String value) {
    for (OBEntryStatus1Code b : OBEntryStatus1Code.values()) {
      if (b.value.equals(value)) {
        return b;
      }
    }
    throw new IllegalArgumentException("Unexpected value '" + value + "'");
  }
}

