package com.manning.openbanking.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * Set of elements used to provide details of a generic date time for the statement resource.
 */

@Schema(name = "OBStatement2_StatementDateTime_inner", description = "Set of elements used to provide details of a generic date time for the statement resource.")
@JsonTypeName("OBStatement2_StatementDateTime_inner")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-01-30T17:41:15.217637300-05:00[America/Bogota]")
public class OBStatement2StatementDateTimeInner {

  @JsonProperty("DateTime")
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
  private Date dateTime;

  @JsonProperty("Type")
  private String type;

  public OBStatement2StatementDateTimeInner dateTime(Date dateTime) {
    this.dateTime = dateTime;
    return this;
  }

  /**
   * Date and time associated with the date time type.All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00
   * @return dateTime
  */
  @NotNull @Valid 
  @Schema(name = "DateTime", description = "Date and time associated with the date time type.All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00", required = true)
  public Date getDateTime() {
    return dateTime;
  }

  public void setDateTime(Date dateTime) {
    this.dateTime = dateTime;
  }

  public OBStatement2StatementDateTimeInner type(String type) {
    this.type = type;
    return this;
  }

  /**
   * Date time type, in a coded form.
   * @return type
  */
  @NotNull 
  @Schema(name = "Type", description = "Date time type, in a coded form.", required = true)
  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OBStatement2StatementDateTimeInner obStatement2StatementDateTimeInner = (OBStatement2StatementDateTimeInner) o;
    return Objects.equals(this.dateTime, obStatement2StatementDateTimeInner.dateTime) &&
        Objects.equals(this.type, obStatement2StatementDateTimeInner.type);
  }

  @Override
  public int hashCode() {
    return Objects.hash(dateTime, type);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OBStatement2StatementDateTimeInner {\n");
    sb.append("    dateTime: ").append(toIndentedString(dateTime)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

