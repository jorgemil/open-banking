package com.manning.openbanking.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonValue;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Specifies the Mutability of the Transaction record.
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-01-30T17:41:15.217637300-05:00[America/Bogota]")
public enum OBTransactionMutability1Code {
  
  MUTABLE("Mutable"),
  
  IMMUTABLE("Immutable");

  private String value;

  OBTransactionMutability1Code(String value) {
    this.value = value;
  }

  @JsonValue
  public String getValue() {
    return value;
  }

  @Override
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static OBTransactionMutability1Code fromValue(String value) {
    for (OBTransactionMutability1Code b : OBTransactionMutability1Code.values()) {
      if (b.value.equals(value)) {
        return b;
      }
    }
    throw new IllegalArgumentException("Unexpected value '" + value + "'");
  }
}

