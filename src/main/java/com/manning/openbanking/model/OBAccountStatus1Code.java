package com.manning.openbanking.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonValue;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Specifies the status of account resource in code form.
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-01-30T17:41:15.217637300-05:00[America/Bogota]")
public enum OBAccountStatus1Code {
  
  DELETED("Deleted"),
  
  DISABLED("Disabled"),
  
  ENABLED("Enabled"),
  
  PENDING("Pending"),
  
  PROFORMA("ProForma");

  private String value;

  OBAccountStatus1Code(String value) {
    this.value = value;
  }

  @JsonValue
  public String getValue() {
    return value;
  }

  @Override
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static OBAccountStatus1Code fromValue(String value) {
    for (OBAccountStatus1Code b : OBAccountStatus1Code.values()) {
      if (b.value.equals(value)) {
        return b;
      }
    }
    throw new IllegalArgumentException("Unexpected value '" + value + "'");
  }
}

