package com.manning.openbanking.web;

import com.manning.openbanking.entity.Transaction;
import com.manning.openbanking.service.TransactionService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path="/transactions", produces = "application/json")
public class TransactionController {

    private final TransactionService transactionService;

    public TransactionController(final TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @GetMapping("/{accountNumber}")
    public List<TransactionDTO> getTransactions(@PathVariable("accountNumber") Long accountNumber){
        List<TransactionDTO> transactionDTOList = new ArrayList<>();

        List<Transaction> allByAccountNumber = transactionService.findAllByAccountNumber(accountNumber);

        for (Transaction transaction : allByAccountNumber) {
            TransactionDTO transactionDTO = new TransactionDTO();
            transactionDTO.setAccountNumber(transaction.getAccountNumber());
            transactionDTO.setAmount(transaction.getAmount());
            transactionDTO.setDate(transaction.getDate());
            transactionDTO.setCurrency(transaction.getCurrency());
            transactionDTOList.add(transactionDTO);
        }

        return transactionDTOList;
    }
}
