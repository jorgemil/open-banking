package com.manning.openbanking.web;

import lombok.Data;

import java.util.Date;

@Data
public class TransactionDTO {
    private String type;
    private Date date;
    private Long accountNumber;
    private String currency;
    private Long amount;
    private String merchantName;
    private String merchantLogo;
}
