package com.manning.openbanking.service;

import com.manning.openbanking.entity.Transaction;
import com.manning.openbanking.repository.TransactionRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionService {

    private final TransactionRepository transactionRepository;

    public TransactionService(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    public List<Transaction> findAllByAccountNumber(Long accountNumber) {
        return transactionRepository.getTransactionsByAccountNumber(accountNumber);
    }
}
