package com.manning.openbanking.web;

import com.manning.openbanking.OpenBankingApplication;
import com.manning.openbanking.repository.TransactionRepository;
import com.manning.openbanking.service.TransactionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;

@SpringBootTest(classes = {OpenBankingApplication.class})
class TransactionComponentTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TransactionRepository transactionRepository;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getTransactions() {
        given().standaloneSetup(new TransactionController(new TransactionService(transactionRepository)))
                .when()
                .get(String.format("http://localhost:%s/transactions/123", port))
                .then()
                .statusCode(200);
    }
}
