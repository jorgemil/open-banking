package com.manning.openbanking.service;

import com.manning.openbanking.OpenBankingApplication;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;

import static org.springframework.test.util.AssertionErrors.assertTrue;

@SpringBootTest(classes = {OpenBankingApplication.class})
@EnableAutoConfiguration
class TransactionServiceTest {

    @Autowired
    private TransactionService transactionService;

    @Test
    void testTransactions(){
        int size = transactionService.findAllByAccountNumber(123L).size();
        assertTrue("Error, there are not 3 transactions", size == 3);
    }
}
