CREATE TABLE transactions (
    id bigint NOT NULL,
    type varchar(255),
    date timestamp,
    account_number bigint,
    currency varchar(255),
    amount bigint,
    merchant_name varchar(255),
    merchant_logo varchar(255),
    PRIMARY KEY (id)
);

INSERT INTO transactions (id, type, date, account_number, currency, amount, merchant_name, merchant_logo) VALUES (1, 'type1', now(), 123, 'usd', 1000, 'merchant name 1', 'merchant logo 1');

INSERT INTO transactions (id, type, date, account_number, currency, amount, merchant_name, merchant_logo) VALUES (2, 'type2', now(), 123, 'usd', 2000, 'merchant name 2', 'merchant logo 2');

INSERT INTO transactions (id, type, date, account_number, currency, amount, merchant_name, merchant_logo) VALUES (3, 'type3', now(), 123, 'usd', 3000, 'merchant name 3', 'merchant logo 3');